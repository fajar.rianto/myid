package com.example.myid.core.presenter.auth

import android.annotation.SuppressLint
import com.example.myid.core.network.Api
import com.example.myid.core.network.getApi
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import retrofit2.HttpException

class AuthPresenter : AuthView.logic {
    private lateinit var viewauth: AuthView.View.auth
    private var api: Api = getApi().create(Api::class.java)

    override fun setupAuthView(view: AuthView.View.auth) {
        this.viewauth = view
    }

    @SuppressLint("CheckResult")
    fun doLogin(phone_number: String?, password: String?) {
        viewauth.showProgress()
        api.doLogin(phone_number, password)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.newThread())
            .subscribe(
                {
                    viewauth.hideProgress()
                    viewauth.whenSuccessLogin(it.success, it.data)
                },
                {
                    viewauth.hideProgress()
                    try {
                        val error: HttpException = it as HttpException
//                        if (error.code() == 500) {
//                            val response = JSONObject()
//                            response.put("message", R.string.CODE_ERROR_500)
//                            viewauth.whenHandleError(error.message(), response)
//                        } else if (error.code() == 429) {
//                            val response = JSONObject()
//                            response.put("message", R.string.CODE_ERROR_429)
//                            viewauth.whenHandleError(error.code(), response)
//                        } else {
//                            var data = JSONObject(error.response()?.errorBody()?.string())
//                            viewauth.whenHandleError(error.code(), data)
//                        }
                    }catch (ex:Exception){

                    }
                }
            )
    }

    @SuppressLint("CheckResult")
    fun doProfile(tokens: String?) {
        viewauth.showProgress()
        api.doProfile(tokens)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.newThread())
            .subscribe(
                {
                    viewauth.hideProgress()
                    viewauth.whenSuccessProfile(it.success, it.data)
                },
                {
                    viewauth.hideProgress()
                    try {
                        val error: HttpException = it as HttpException
//                        if (error.code() == 500) {
//                            val response = JSONObject()
//                            response.put("message", R.string.CODE_ERROR_500)
//                            viewauth.whenHandleError(error.message(), response)
//                        } else if (error.code() == 429) {
//                            val response = JSONObject()
//                            response.put("message", R.string.CODE_ERROR_429)
//                            viewauth.whenHandleError(error.code(), response)
//                        } else {
//                            var data = JSONObject(error.response()?.errorBody()?.string())
//                            viewauth.whenHandleError(error.code(), data)
//                        }
                    }catch (ex:Exception){

                    }
                }
            )
    }

    @SuppressLint("CheckResult")
    fun doRegister(domain: String?, full_name: String?, nickname: String?, phone_number: String?,
                   email: String?, password: String?, otp_number: String?) {
        viewauth.showProgress()
        api.doRegister(domain, full_name, nickname, phone_number, email, password, otp_number)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.newThread())
            .subscribe(
                {
                    viewauth.hideProgress()
                    viewauth.whenSuccessRegister(it.success, it.data)
                },
                {
                    viewauth.hideProgress()
                    try {
                        val error: HttpException = it as HttpException
//                        if (error.code() == 500) {
//                            val response = JSONObject()
//                            response.put("message", R.string.CODE_ERROR_500)
//                            viewauth.whenHandleError(error.message(), response)
//                        } else if (error.code() == 429) {
//                            val response = JSONObject()
//                            response.put("message", R.string.CODE_ERROR_429)
//                            viewauth.whenHandleError(error.code(), response)
//                        } else {
//                            var data = JSONObject(error.response()?.errorBody()?.string())
//                            viewauth.whenHandleError(error.code(), data)
//                        }
                    }catch (ex:Exception){

                    }
                }
            )
    }

    @SuppressLint("CheckResult")
    fun doSendOtpRegister(phone_number: String?, email: String?) {
        viewauth.showProgress()
        api.doSendOtpRegsiter(phone_number, email)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.newThread())
            .subscribe(
                {
                    viewauth.hideProgress()
                    viewauth.whenSuccessOtp(it.success, it.message, null)
                },
                {
                    viewauth.hideProgress()
                    try {
                        val error: HttpException = it as HttpException
//                        if (error.code() == 500) {
//                            val response = JSONObject()
//                            response.put("message", R.string.CODE_ERROR_500)
//                            viewauth.whenHandleError(error.message(), response)
//                        } else if (error.code() == 429) {
//                            val response = JSONObject()
//                            response.put("message", R.string.CODE_ERROR_429)
//                            viewauth.whenHandleError(error.code(), response)
//                        } else {
//                            var data = JSONObject(error.response()?.errorBody()?.string())
//                            viewauth.whenHandleError(error.code(), data)
//                        }
                    }catch (ex:Exception){

                    }
                }
            )
    }

    @SuppressLint("CheckResult")
    fun doGetOtpForgotPassword(phone_number: String?, domain: String?) {
        viewauth.showProgress()
        api.doGetOtpForgotPassword(phone_number, domain)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.newThread())
            .subscribe(
                {
                    viewauth.hideProgress()
                    viewauth.whenSuccessOtp(it.success, it.message, null)
                },
                {
                    viewauth.hideProgress()
                    try {
                        val error: HttpException = it as HttpException
//                        if (error.code() == 500) {
//                            val response = JSONObject()
//                            response.put("message", R.string.CODE_ERROR_500)
//                            viewauth.whenHandleError(error.message(), response)
//                        } else if (error.code() == 429) {
//                            val response = JSONObject()
//                            response.put("message", R.string.CODE_ERROR_429)
//                            viewauth.whenHandleError(error.code(), response)
//                        } else {
//                            var data = JSONObject(error.response()?.errorBody()?.string())
//                            viewauth.whenHandleError(error.code(), data)
//                        }
                    }catch (ex:Exception){

                    }
                }
            )
    }


    @SuppressLint("CheckResult")
    fun doSendValidateOtpForgotPassword(new_password: String?, phone_number: String?) {
        viewauth.showProgress()
        api.doSendValidateOtpForgotPassword(new_password, phone_number)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.newThread())
            .subscribe(
                {
                    viewauth.hideProgress()
                    viewauth.whenSuccessOtp(it.success, it.message, it.data)
                },
                {
                    viewauth.hideProgress()
                    try {
                        val error: HttpException = it as HttpException
//                        if (error.code() == 500) {
//                            val response = JSONObject()
//                            response.put("message", R.string.CODE_ERROR_500)
//                            viewauth.whenHandleError(error.message(), response)
//                        } else if (error.code() == 429) {
//                            val response = JSONObject()
//                            response.put("message", R.string.CODE_ERROR_429)
//                            viewauth.whenHandleError(error.code(), response)
//                        } else {
//                            var data = JSONObject(error.response()?.errorBody()?.string())
//                            viewauth.whenHandleError(error.code(), data)
//                        }
                    }catch (ex:Exception){

                    }
                }
            )
    }

    @SuppressLint("CheckResult")
    fun doSendNewForgotPassword(new_password: String?, phone_number: String?, token: String?) {
        viewauth.showProgress()
        api.doSendNewForgotPassword(new_password, phone_number, token)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.newThread())
            .subscribe(
                {
                    viewauth.hideProgress()
                    viewauth.whenSuccessOtp(it.success, it.message, it.data)
                },
                {
                    viewauth.hideProgress()
                    try {
                        val error: HttpException = it as HttpException
//                        if (error.code() == 500) {
//                            val response = JSONObject()
//                            response.put("message", R.string.CODE_ERROR_500)
//                            viewauth.whenHandleError(error.message(), response)
//                        } else if (error.code() == 429) {
//                            val response = JSONObject()
//                            response.put("message", R.string.CODE_ERROR_429)
//                            viewauth.whenHandleError(error.code(), response)
//                        } else {
//                            var data = JSONObject(error.response()?.errorBody()?.string())
//                            viewauth.whenHandleError(error.code(), data)
//                        }
                    }catch (ex:Exception){

                    }
                }
            )
    }

    @SuppressLint("CheckResult")
    fun doUpdateProfilePicture(token: String?, profil_pict: String?) {
        viewauth.showProgress()
        api.doUpdateProfilPicture(token, profil_pict)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.newThread())
            .subscribe(
                {
                    viewauth.hideProgress()
                    viewauth.whenSuccessProfile(it.success, it.data)
                },
                {
                    viewauth.hideProgress()
                    try {
                        val error: HttpException = it as HttpException
//                        if (error.code() == 500) {
//                            val response = JSONObject()
//                            response.put("message", R.string.CODE_ERROR_500)
//                            viewauth.whenHandleError(error.message(), response)
//                        } else if (error.code() == 429) {
//                            val response = JSONObject()
//                            response.put("message", R.string.CODE_ERROR_429)
//                            viewauth.whenHandleError(error.code(), response)
//                        } else {
//                            var data = JSONObject(error.response()?.errorBody()?.string())
//                            viewauth.whenHandleError(error.code(), data)
//                        }
                    }catch (ex:Exception){

                    }
                }
            )
    }
}