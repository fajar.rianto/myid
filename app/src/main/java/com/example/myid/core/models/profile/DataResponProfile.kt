package com.example.myid.core.models.profile

data class DataResponProfile(
    val address: String,
    val email: String,
    val full_name: String,
    val gender: String,
    val nickname: String,
    val phone_number: String,
    val profile_pict: String
)