package com.example.myid.core.presenter.auth

import com.example.myid.core.models.auth.DataResponAuth
import com.example.myid.core.models.otp.DataResponOtp
import com.example.myid.core.models.otp.ResponOtp
import com.example.myid.core.models.profile.DataResponProfile
import com.example.myid.extentions.BaseView

interface AuthView{
    interface View : BaseView.progressView{

        interface auth : BaseView.progressView {
            fun whenSuccessLogin(success : Boolean?, data: DataResponAuth)
            fun whenSuccessVerifikasi(success : Boolean?, data: DataResponAuth)
            fun whenSuccessRegister(success : Boolean?, data: DataResponAuth)
            fun whenSuccessForgotPassword(status: Boolean?, data: ResponOtp)
            fun whenSuccessOtp(status: Boolean?, message: String?, data: DataResponOtp?)
            fun whenSuccessProfile(success : Boolean?, data: DataResponProfile)
            fun  whenHandleError(success : Boolean?, error: String?)
        }
    }

    interface logic{

        fun setupAuthView(view: View.auth)

    }
}