package com.example.mettadc.core.models.intro

class OnBoardingItem {
    var image = 0
    var title: String? = null
    var description: String? = null
}