package com.example.myid.core.models.auth

data class ResponAuth(
    val data: DataResponAuth,
    val error: String,
    val message: String,
    val success: Boolean
)