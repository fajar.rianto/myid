package com.example.myid.core.adapter.fragment.home

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.mettadc.core.models.dashboard.slide.DataSlideInspirasi
import com.example.myid.core.models.dashboard.slide.DataSlideNews
import com.example.myid.databinding.ItemNewsBinding
import com.github.islamkhsh.CardSliderAdapter

class SliderNewsAdapter(private val movies: List<DataSlideNews>,
                        private val mContext: Context
) : CardSliderAdapter<SliderNewsAdapter.ViewHolderList>() {


    override fun bindVH(holder: ViewHolderList, position: Int) {
        val movie = movies[position]
        holder.binding.tvImage.setImageResource(movie.image)
    }


    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolderList {
        val binding = ItemNewsBinding.inflate(
            LayoutInflater.from(mContext),
            parent,
            false
        )
        return ViewHolderList(binding)
    }

    override fun getItemCount() = movies.size

    class ViewHolderList(val binding: ItemNewsBinding) : RecyclerView.ViewHolder(binding.root)
}