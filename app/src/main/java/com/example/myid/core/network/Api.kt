package com.example.myid.core.network

import com.example.myid.core.models.auth.ResponAuth
import com.example.myid.core.models.otp.ResponOtp
import com.example.myid.core.models.profile.ResponProfile
import io.reactivex.Single
import retrofit2.http.*


interface Api {

    @POST("v1/auth/login")
    fun doLogin(
        @Query("phone_number") phone_number: String?,
        @Query("password") password: String?,
    ): Single<ResponAuth>

    @POST("v1/auth/register")
    fun doRegister(
        @Query("domain") domain: String?,
        @Query("full_name") full_name: String?,
        @Query("nickname") nickname: String?,
        @Query("phone_number") phone_number: String?,
        @Query("email") email: String?,
        @Query("password") password: String?,
        @Query("otp_number") otp_number: String?,
    ): Single<ResponAuth>

    @GET("v1/auth/profile")
    fun doProfile(
        @Header("Authorization") tokens: String?,
    ): Single<ResponProfile>

    @POST("v1/auth/send-otp/register")
    fun doSendOtpRegsiter(
        @Query("phone_number") phone_number: String?,
        @Query("email") email: String?
    ): Single<ResponOtp>

    @POST("v1/auth/send-otp/forgot-password")
    fun doGetOtpForgotPassword(
        @Query("phone_number") phone_number: String?,
        @Query("domain") domain: String?
    ): Single<ResponOtp>

    @POST("/v1/auth/forgot-password")
    fun doSendValidateOtpForgotPassword(
        @Query("otp_number") otp_number: String?,
        @Query("phone_number") phone_number: String?,
    ): Single<ResponOtp>

    @POST("/v1/auth/new-password")
    fun doSendNewForgotPassword(
        @Query("new_password") new_password: String?,
        @Query("phone_number") phone_number: String?,
        @Query("token") token: String?
    ): Single<ResponOtp>


    @POST("v1/auth/profile-picture")
    fun doUpdateProfilPicture(
        @Header("Authorization") token: String?,
        @Query("profil_pict") profil_pict: String?,
    ): Single<ResponProfile>

    @GET("v1/auth/refresh-token")
    fun doRefreshToken(
        @Header("Authorization") token: String?,
    ): Single<ResponAuth>


}