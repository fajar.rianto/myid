package com.example.myid.core.models.auth

data class DataResponAuth(
    val access_token: String,
    val refresh_token: String
)