package com.example.myid.core.models.otp

data class ResponOtp(
    val error: String,
    val message: String,
    val success: Boolean,
    val data: DataResponOtp
)