package com.example.myid.core.adapter.fragment.home

import android.R
import android.content.Context
import android.graphics.drawable.Drawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.mettadc.core.models.dashboard.slide.DataSlideInspirasi
import com.example.myid.databinding.ItemSliderInspirasiBinding
import com.github.islamkhsh.CardSliderAdapter


class SliderInspirasiAdapter(private val movies: List<DataSlideInspirasi>,
                             private val mContext: Context
) : CardSliderAdapter<SliderInspirasiAdapter.ViewHolderList>() {


    override fun bindVH(holder: ViewHolderList, position: Int) {
        val movie = movies[position]
        holder.binding.tvImage.setImageResource(movie.image)
    }


    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolderList {
        val binding = ItemSliderInspirasiBinding.inflate(
            LayoutInflater.from(mContext),
            parent,
            false
        )
        return ViewHolderList(binding)
    }

    override fun getItemCount() = movies.size

    class ViewHolderList(val binding: ItemSliderInspirasiBinding) : RecyclerView.ViewHolder(binding.root)
}