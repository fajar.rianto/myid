package com.example.myid.core.models.profile

data class ResponProfile(
    val `data`: DataResponProfile,
    val message: String,
    val success: Boolean
)