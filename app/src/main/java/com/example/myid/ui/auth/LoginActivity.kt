package com.example.myid.ui.auth

import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.myid.R
import com.example.myid.core.models.auth.DataResponAuth
import com.example.myid.core.models.otp.DataResponOtp
import com.example.myid.core.models.otp.ResponOtp
import com.example.myid.core.models.profile.DataResponProfile
import com.example.myid.core.presenter.auth.AuthPresenter
import com.example.myid.core.presenter.auth.AuthView
import com.example.myid.databinding.ActivityLoginBinding
import com.example.myid.extentions.CustomProgressDialog
import com.example.myid.extentions.SharedPreference
import com.example.myid.ui.dashboard.DashboardActivity
import com.google.android.gms.common.api.ApiException


class LoginActivity : AppCompatActivity(), AuthView.View.auth {
    private lateinit var binding: ActivityLoginBinding
    private var authPresenter = AuthPresenter()
    private lateinit var sharedPreference: SharedPreference
    private val progressDialog = CustomProgressDialog()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLoginBinding.inflate(layoutInflater)
        setContentView(binding.root)

        sharedPreference = SharedPreference(this)
        authPresenter.setupAuthView(this)

        binding.txPhone.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
            override fun afterTextChanged(s: Editable) {
                if (s.toString().length == 1 && s.toString().startsWith("0")) {
                    s.clear()
                }
            }
        })

        binding.btnForgotPassword.setOnClickListener {
            val intent = Intent(this, ForgotPasswordActivity::class.java)
            startActivity(intent)
        }

        binding.btnRegister.setOnClickListener {
            val intent = Intent(this, RegisterActivity::class.java)
            startActivity(intent)
        }

        binding.btnLogin.setOnClickListener {
            var get_phone    = binding.txPhone.text.toString()
            var get_password = binding.txPassword.text.toString()
            if (get_phone.equals("")) {
                Toast.makeText(this, "Masukan nomor telepon", Toast.LENGTH_SHORT).show()
            }else if (get_password.equals("")){
                Toast.makeText(this, "Masukan password", Toast.LENGTH_SHORT).show()
            }else{
                authPresenter.doLogin(get_phone, get_password)
            }

        }
    }

    override fun whenSuccessLogin(success: Boolean?, data: DataResponAuth) {
        try {
            sharedPreference.save(getString(R.string.PREF_SESSION), true)
            sharedPreference.save(getString(R.string.PREF_TOKEN), data.access_token)
            sharedPreference.save(getString(R.string.PREF_REFRESH_TOKEN), data.refresh_token)
            val intent = Intent(this, DashboardActivity::class.java)
            startActivity(intent)
        } catch (e: ApiException){
            Toast.makeText(this,e.toString(), Toast.LENGTH_SHORT).show()
        }
    }

    override fun whenSuccessVerifikasi(success: Boolean?, data: DataResponAuth) {
        TODO("Not yet implemented")
    }

    override fun whenSuccessRegister(success: Boolean?, data: DataResponAuth) {
        TODO("Not yet implemented")
    }

    override fun whenSuccessForgotPassword(status: Boolean?, data: ResponOtp) {
        TODO("Not yet implemented")
    }

    override fun whenSuccessOtp(status: Boolean?, message: String?, data: DataResponOtp?) {
        TODO("Not yet implemented")
    }

    override fun whenSuccessProfile(success: Boolean?, data: DataResponProfile) {
        TODO("Not yet implemented")
    }

    override fun whenHandleError(success: Boolean?, error: String?) {
        TODO("Not yet implemented")
    }

    override fun showProgress() {
        progressDialog.show(this,"")
    }

    override fun hideProgress() {
        progressDialog.dialog_custom.dismiss()
    }

}