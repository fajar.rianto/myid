package com.example.myid.ui.dashboard.fragment.home

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.mettadc.core.models.dashboard.slide.DataSlideInspirasi
import com.example.myid.R
import com.example.myid.core.adapter.fragment.home.SliderInspirasiAdapter
import com.example.myid.core.adapter.fragment.home.SliderNewsAdapter
import com.example.myid.core.models.dashboard.slide.DataSlideNews
import com.github.islamkhsh.CardSliderViewPager

class FragmentHome : Fragment(){
    private lateinit var mView: View


    @SuppressLint("Recycle")
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        mView = inflater.inflate(R.layout.fragment_home, container, false)


        val dataslider = arrayListOf<DataSlideInspirasi>()
        dataslider.clear()
        val posters = resources.obtainTypedArray(R.array.image_slider)
        for (i in resources.getStringArray(R.array.image_slider).indices) {
            dataslider.add(
                DataSlideInspirasi(
                    posters.getResourceId(i, -1)
                )
            )
        }

        val dataslider_news = arrayListOf<DataSlideNews>()
        dataslider_news.clear()
        val posters_news = resources.obtainTypedArray(R.array.image_slider_news)
        for (i in resources.getStringArray(R.array.image_slider_news).indices) {
            dataslider_news.add(
                DataSlideNews(
                    posters_news.getResourceId(i, -1)
                )
            )
        }


        mView.findViewById<CardSliderViewPager>(R.id.viewPager).adapter = SliderInspirasiAdapter(dataslider, requireContext())

        mView.findViewById<CardSliderViewPager>(R.id.viewPager_news_one).adapter = SliderNewsAdapter(dataslider_news, requireContext())


        return mView
    }
}