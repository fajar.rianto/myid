package com.example.myid.ui.auth

import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.widget.ImageView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.myid.R
import com.example.myid.core.models.auth.DataResponAuth
import com.example.myid.core.models.otp.DataResponOtp
import com.example.myid.core.models.otp.ResponOtp
import com.example.myid.core.models.profile.DataResponProfile
import com.example.myid.core.presenter.auth.AuthPresenter
import com.example.myid.core.presenter.auth.AuthView
import com.example.myid.databinding.ActivityRegisterBinding
import com.example.myid.extentions.CustomProgressDialogFull
import com.google.android.gms.common.api.ApiException


class RegisterActivity : AppCompatActivity(), AuthView.View.auth {
    private lateinit var binding: ActivityRegisterBinding
    private var authPresenter = AuthPresenter()
    private val progressDialog = CustomProgressDialogFull()
    var is_sk     = false
    var is_inform = false
    var message_password = ""
    var is_valid  = false
    lateinit var btn_back: ImageView

    var set_domain   = ""
    var set_name     = ""
    var set_email    = ""
    var set_phome    = ""
    var set_password = ""
    var set_confirm_password = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityRegisterBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.btnRegister.isEnabled = false
        binding.btnRegister.setBackgroundResource(R.drawable.bg_button_disable)
        btn_back = findViewById(R.id.btn_back)
        btn_back.setOnClickListener {
            val intent = Intent(this, LoginActivity::class.java)
            startActivity(intent)
        }


        binding.txPhone.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
            override fun afterTextChanged(s: Editable) {
                if (s.toString().length == 1 && s.toString().startsWith("0")) {
                    s.clear()
                }
            }
        })

        binding.syaratketentuanbox.setOnCheckedChangeListener { _, isChecked ->
            is_sk = isChecked
            if (is_sk) {
                if (is_inform) {
                    binding.btnRegister.isEnabled = true
                    binding.btnRegister.setBackgroundResource(R.drawable.bg_button_enable)
                } else {
                    binding.btnRegister.isEnabled = false
                    binding.btnRegister.setBackgroundResource(R.drawable.bg_button_disable)
                }
            } else {
                binding.btnRegister.isEnabled = false
                binding.btnRegister.setBackgroundResource(R.drawable.bg_button_disable)
            }
        }

        binding.informcheckbox.setOnCheckedChangeListener { _, isChecked ->
            is_inform = isChecked
            if (is_inform) {
                if (is_sk) {
                    binding.btnRegister.isEnabled = true
                    binding.btnRegister.setBackgroundResource(R.drawable.bg_button_enable)
                } else {
                    binding.btnRegister.isEnabled = false
                    binding.btnRegister.setBackgroundResource(R.drawable.bg_button_disable)
                }
            } else {
                binding.btnRegister.isEnabled = false
                binding.btnRegister.setBackgroundResource(R.drawable.bg_button_disable)
            }
        }

        authPresenter.setupAuthView(this)
        binding.btnRegister.setOnClickListener {
            var get_password = binding.txPassword.text.toString()
            var get_confirm_password = binding.txPassword.text.toString()
            isValidPassword(get_password)
            if (is_valid) {
                if (get_password == get_confirm_password){
                    set_domain   = binding.txDomain.text.toString()+".my.id"
                    set_name     = binding.txName.text.toString()
                    set_email    = binding.txEmail.text.toString()
                    set_phome    = "62"+binding.txPhone.text.toString()
                    set_password = binding.txPassword.text.toString()
                    set_confirm_password = binding.txConfirmPassword.text.toString()

                    if (set_domain.equals("")) {
                        Toast.makeText(this, "Masukan domain anda", Toast.LENGTH_SHORT).show()
                    }else if (set_name.equals("")){
                        Toast.makeText(this, "Masukan nama anda", Toast.LENGTH_SHORT).show()
                    }else if (set_email.equals("")){
                        Toast.makeText(this, "Masukan email anda", Toast.LENGTH_SHORT).show()
                    }else if (set_phome.equals("")){
                        Toast.makeText(this, "Masukan nomor telepon", Toast.LENGTH_SHORT).show()
                    }else{
                        authPresenter.doSendOtpRegister(set_phome, set_email)
                    }

                }else{
                    Toast.makeText(this, "Password tidak sama", Toast.LENGTH_SHORT).show()
                }
            } else {
                Toast.makeText(this, "$message_password", Toast.LENGTH_SHORT).show()
            }
        }

    }

    fun isValidPassword(password: String) {

        val validate_uppercase = ".*[A-Z].*".toRegex()
        val validate_digit = ".*[0-9].*".toRegex()
        val validate_caracter = ".*[@#!$%^&+=].*".toRegex()

        val is_upper = password.matches(validate_uppercase)
        val is_digit = password.matches(validate_digit)
        val is_caracter = password.matches(validate_caracter)

        if (password.length < 6) {
            message_password = "Password lenght must have alleast 6 character !!"
            is_valid = false
        } else {
            if (is_upper) {
                if (is_digit) {
                    if (is_caracter) {
                        is_valid = true
                        message_password = "password secure is good"
                    } else {
                        message_password = "Password must have special characters !!"
                        is_valid = false
                    }
                } else {
                    is_valid = false
                    message_password = "Password must have atleast one digit character !!"
                }
            } else {
                is_valid = false
                message_password = "Password must have atleast one uppercase character !!"
            }
        }
    }


    override fun whenSuccessRegister(success: Boolean?, data: DataResponAuth) {

    }

    override fun whenSuccessLogin(success: Boolean?, data: DataResponAuth) {
        TODO("Not yet implemented")
    }

    override fun whenSuccessVerifikasi(success: Boolean?, data: DataResponAuth) {
        TODO("Not yet implemented")
    }

    override fun whenSuccessForgotPassword(status: Boolean?, data: ResponOtp) {
        TODO("Not yet implemented")
    }

    override fun whenSuccessOtp(status: Boolean?, message: String?, data: DataResponOtp?) {
        try {
            val intent = Intent(this, OtpRegisterActivity::class.java)
            intent.putExtra("domain", set_domain)
            intent.putExtra("name", set_name)
            intent.putExtra("email", set_email)
            intent.putExtra("phone", set_phome)
            intent.putExtra("password", set_password)
            intent.putExtra("confirm_password", set_confirm_password)
            startActivity(intent)
        } catch (e: ApiException){
            Toast.makeText(this,e.toString(), Toast.LENGTH_SHORT).show()
        }
    }

    override fun whenSuccessProfile(success: Boolean?, data: DataResponProfile) {
        TODO("Not yet implemented")
    }

    override fun whenHandleError(success: Boolean?, error: String?) {
        TODO("Not yet implemented")
    }


    override fun showProgress() {
        progressDialog.show(this,"")
    }

    override fun hideProgress() {
        progressDialog.dialog_custom.dismiss()
    }

}