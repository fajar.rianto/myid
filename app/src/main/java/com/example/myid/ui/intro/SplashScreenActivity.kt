package com.example.myid.ui.intro

import android.annotation.SuppressLint
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.WindowManager
import com.example.myid.R
import com.example.myid.databinding.ActivitySplashScreenBinding
import com.example.myid.extentions.SharedPreference
import com.example.myid.ui.dashboard.DashboardActivity

@SuppressLint("CustomSplashScreen")
class SplashScreenActivity : AppCompatActivity() {
    private lateinit var binding: ActivitySplashScreenBinding
    private lateinit var sharedPreference: SharedPreference

    var is_onboarding = false
    var is_term       = false
    var is_session    = false


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySplashScreenBinding.inflate(layoutInflater)
        setContentView(binding.root)

        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )

        Handler(Looper.getMainLooper()).postDelayed({

            sharedPreference = SharedPreference(this)
            is_onboarding    = sharedPreference.getValueBoolien(getString(R.string.PREF_ONBOARDING), false)
            is_term          = sharedPreference.getValueBoolien(getString(R.string.PREF_TERM), false)
            is_session       = sharedPreference.getValueBoolien(getString(R.string.PREF_SESSION), false)

            if (is_onboarding){
                if (is_session){
                    startActivity(Intent(applicationContext, DashboardActivity::class.java))
                    finish()
                }else{
                    startActivity(Intent(applicationContext, StartedActivity::class.java))
                    finish()
                }

            }else{
                val intent = Intent(this, OnBoardingActivity::class.java)
                startActivity(intent)
                finish()
            }
        }, 3000)

    }
}