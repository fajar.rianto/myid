package com.example.myid.ui.auth

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.myid.databinding.ActivityPreviewProfileBinding

class SummaryUserActivity : AppCompatActivity() {
    private lateinit var binding: ActivityPreviewProfileBinding

    var get_domain = ""
    var get_name   = ""
    var get_email  = ""
    var get_phome  = ""
    var get_password  = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityPreviewProfileBinding.inflate(layoutInflater)
        setContentView(binding.root)

        get_domain = intent.getStringExtra("domain").toString()
        get_name = intent.getStringExtra("name").toString()
        get_email = intent.getStringExtra("email").toString()
        get_password = intent.getStringExtra("password").toString()
        get_phome = intent.getStringExtra("phone").toString()

        binding.tVdomain.text    = ": $get_domain"
        binding.tvEmail.text     = ": $get_email"
        binding.tvNama.text      = ": $get_name"
        binding.tvHandphone.text = ": $get_phome"
        binding.tvPassword.text  = ": ${get_password.substring(0, 4)}****"

        binding.btnSelesai.setOnClickListener {
            startActivity(Intent(this, LoginActivity::class.java))
        }
    }
}