package com.example.myid.ui.auth

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.myid.R
import com.example.myid.databinding.ActivityNewPasswordBinding
import com.example.myid.databinding.ActivityNewPasswordSuccessBinding

class NewPasswordSuccessActivity : AppCompatActivity() {
    private lateinit var binding: ActivityNewPasswordSuccessBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityNewPasswordSuccessBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.btnSelesai.setOnClickListener {
            val intent = Intent(this, LoginActivity::class.java)
            startActivity(intent)
        }
    }
}