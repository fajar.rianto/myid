package com.example.myid.ui.auth

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.CountDownTimer
import android.widget.Toast
import com.example.myid.core.models.auth.DataResponAuth
import com.example.myid.core.models.otp.DataResponOtp
import com.example.myid.core.models.otp.ResponOtp
import com.example.myid.core.models.profile.DataResponProfile
import com.example.myid.core.presenter.auth.AuthPresenter
import com.example.myid.core.presenter.auth.AuthView
import com.example.myid.databinding.ActivityOtpForgotPasswordBinding
import com.example.myid.extentions.CustomProgressDialogFull
import com.google.android.gms.common.api.ApiException

class OtpForgotPasswordActivity : AppCompatActivity(), AuthView.View.auth {
    private lateinit var binding: ActivityOtpForgotPasswordBinding
    private var authPresenter = AuthPresenter()
    private val progressDialog = CustomProgressDialogFull()

    var get_phone  = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityOtpForgotPasswordBinding.inflate(layoutInflater)
        setContentView(binding.root)

        object : CountDownTimer(60000, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                binding.tvCountdown.text = "${millisUntilFinished / 1000}"
            }
            override fun onFinish() {
                binding.tvCountdown.text = "get kode otp!"
            }
        }.start()

        authPresenter.setupAuthView(this)
        get_phone = intent.getStringExtra("phone").toString()
        binding.tvPhone.text = get_phone
        binding.btnVerifikasi.setOnClickListener {
            var code_otp = binding.otpView.otp
            if (code_otp.equals("")){
                Toast.makeText(this, "Masukan kode otp", Toast.LENGTH_SHORT).show()
            }else{
                authPresenter.doSendValidateOtpForgotPassword(code_otp , get_phone)
            }
        }
    }

    override fun whenSuccessLogin(success: Boolean?, data: DataResponAuth) {
        TODO("Not yet implemented")
    }

    override fun whenSuccessVerifikasi(success: Boolean?, data: DataResponAuth) {
        TODO("Not yet implemented")
    }

    override fun whenSuccessRegister(success: Boolean?, data: DataResponAuth) {
        TODO("Not yet implemented")
    }

    override fun whenSuccessForgotPassword(status: Boolean?, data: ResponOtp) {
        TODO("Not yet implemented")
    }

    override fun whenSuccessOtp(status: Boolean?, message: String?, data: DataResponOtp?) {
        try {
            val intent = Intent(this, NewPasswordActivity::class.java)
            intent.putExtra("phone", get_phone)
            intent.putExtra("token", data?.new_password_token)
            startActivity(intent)
        } catch (e: ApiException){
            Toast.makeText(this,e.toString(), Toast.LENGTH_SHORT).show()
        }
    }

    override fun whenSuccessProfile(success: Boolean?, data: DataResponProfile) {
        TODO("Not yet implemented")
    }

    override fun whenHandleError(success: Boolean?, error: String?) {
        TODO("Not yet implemented")
    }

    override fun showProgress() {
        progressDialog.show(this,"")
    }

    override fun hideProgress() {
        progressDialog.dialog_custom.dismiss()
    }
}