package com.example.myid.ui.intro

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.myid.R
import com.example.myid.databinding.ActivitySplashScreenBinding
import com.example.myid.databinding.ActivityStartedBinding
import com.example.myid.ui.auth.LoginActivity
import com.example.myid.ui.auth.RegisterActivity
import com.example.myid.ui.auth.SummaryUserActivity

class StartedActivity : AppCompatActivity() {
    private lateinit var binding: ActivityStartedBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityStartedBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.btnRegis.setOnClickListener {
            startActivity(Intent(this, RegisterActivity::class.java))
        }

        binding.txtSignin.setOnClickListener {
            startActivity(Intent(this, LoginActivity::class.java))
        }
    }
}