package com.example.myid.ui.dashboard

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.Fragment
import com.example.myid.R
import com.example.myid.databinding.ActivityDashboardBinding
import com.example.myid.extentions.SharedPreference
import com.example.myid.ui.dashboard.fragment.account.FragmentAccount
import com.example.myid.ui.dashboard.fragment.home.FragmentHome
import com.example.myid.ui.dashboard.fragment.statistik.FragmentStatistik

class DashboardActivity : AppCompatActivity() {
    private lateinit var binding: ActivityDashboardBinding
    private lateinit var sharedPreference: SharedPreference

    private fun addFragment(fragment: Fragment) {
        supportFragmentManager
            .beginTransaction()
            .setCustomAnimations(
                R.anim.fade_in,
                R.anim.fade_out
            )
            .replace(R.id.content, fragment, fragment.javaClass.simpleName)
            .commit()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDashboardBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.bottomNavigation.setOnNavigationItemSelectedListener { item ->
            when (item.itemId) {
                R.id.menu_home -> {
                    val fragment = FragmentHome()
                    addFragment(fragment)
                }
                R.id.menu_statistik -> {
                    val fragment = FragmentStatistik()
                    addFragment(fragment)
                }
                R.id.menu_user -> {
                    val fragment = FragmentAccount()
                    addFragment(fragment)
                }
            }
            true
        }

    }

    override fun onResume() {
        super.onResume()
        val fragment = FragmentHome()
        addFragment(fragment)
    }

}