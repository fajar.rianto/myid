package com.example.myid.ui.auth

import android.content.Intent
import android.os.Bundle
import android.os.CountDownTimer
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.myid.core.models.auth.DataResponAuth
import com.example.myid.core.models.otp.DataResponOtp
import com.example.myid.core.models.otp.ResponOtp
import com.example.myid.core.models.profile.DataResponProfile
import com.example.myid.core.presenter.auth.AuthPresenter
import com.example.myid.core.presenter.auth.AuthView
import com.example.myid.databinding.ActivityOtpRegisterBinding
import com.example.myid.extentions.CustomProgressDialogFull
import com.google.android.gms.common.api.ApiException


class OtpRegisterActivity : AppCompatActivity(), AuthView.View.auth {
    private lateinit var binding: ActivityOtpRegisterBinding
    private var authPresenter = AuthPresenter()
    private val progressDialog = CustomProgressDialogFull()
    var get_domain = ""
    var get_name   = ""
    var get_email  = ""
    var get_phome  = ""
    var get_password  = ""
    var get_confirm_password  = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityOtpRegisterBinding.inflate(layoutInflater)
        setContentView(binding.root)

        authPresenter.setupAuthView(this)
        object : CountDownTimer(60000, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                binding.tvCountdown.text = "${millisUntilFinished / 1000}"
            }
            override fun onFinish() {
                binding.tvCountdown.text = "get kode otp!"
            }
        }.start()

        get_domain = intent.getStringExtra("domain").toString()
        get_name = intent.getStringExtra("name").toString()
        get_email = intent.getStringExtra("email").toString()
        get_password = intent.getStringExtra("password").toString()
        get_confirm_password = intent.getStringExtra("confirm_password").toString()
        get_phome = intent.getStringExtra("phone").toString()
        binding.tvPhone.setText(get_phome)

        binding.btnVerifikasi.setOnClickListener {
            var code_otp = binding.otpView.otp
            if (code_otp.equals("")){
                Toast.makeText(this, "Masukan kode otp", Toast.LENGTH_SHORT).show()
            }else{
                authPresenter.doRegister(get_domain , get_name, get_name, get_phome,
                    get_email, get_password, code_otp)
            }
//            startActivity(Intent(this, SummaryUserActivity::class.java))
        }
    }

    override fun whenSuccessLogin(success: Boolean?, data: DataResponAuth) {
        TODO("Not yet implemented")
    }

    override fun whenSuccessVerifikasi(success: Boolean?, data: DataResponAuth) {
        TODO("Not yet implemented")
    }

    override fun whenSuccessRegister(success: Boolean?, data: DataResponAuth) {
        try {
            val intent = Intent(this, SummaryUserActivity::class.java)
            intent.putExtra("domain", get_domain)
            intent.putExtra("name", get_name)
            intent.putExtra("email", get_email)
            intent.putExtra("phone", get_phome)
            intent.putExtra("password", get_password)
            startActivity(intent)
        } catch (e: ApiException){
            Toast.makeText(this,e.toString(), Toast.LENGTH_SHORT).show()
        }
    }

    override fun whenSuccessForgotPassword(status: Boolean?, data: ResponOtp) {
        TODO("Not yet implemented")
    }

    override fun whenSuccessOtp(status: Boolean?, message: String?, data: DataResponOtp?) {
        TODO("Not yet implemented")
    }

    override fun whenSuccessProfile(success: Boolean?, data: DataResponProfile) {
        TODO("Not yet implemented")
    }

    override fun whenHandleError(success: Boolean?, error: String?) {
        TODO("Not yet implemented")
    }

    override fun showProgress() {
        progressDialog.show(this,"")
    }

    override fun hideProgress() {
        progressDialog.dialog_custom.dismiss()
    }
}