package com.example.myid.ui.dashboard.fragment.account

import android.content.Intent
import android.os.Bundle
import android.util.Log.d
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.example.myid.R
import com.example.myid.core.models.auth.DataResponAuth
import com.example.myid.core.models.otp.DataResponOtp
import com.example.myid.core.models.otp.ResponOtp
import com.example.myid.core.models.profile.DataResponProfile
import com.example.myid.core.presenter.auth.AuthPresenter
import com.example.myid.core.presenter.auth.AuthView
import com.example.myid.extentions.CustomProgressDialog
import com.example.myid.extentions.SharedPreference
import com.example.myid.ui.auth.ForgotPasswordActivity
import com.example.myid.ui.dashboard.DashboardActivity
import com.example.myid.ui.intro.StartedActivity
import com.google.android.gms.common.api.ApiException
import com.google.android.material.dialog.MaterialAlertDialogBuilder

class FragmentAccount : Fragment(), AuthView.View.auth {
    private var authPresenter = AuthPresenter()
    private lateinit var sharedPreference: SharedPreference
    private val progressDialog = CustomProgressDialog()
    private lateinit var mView: View
    lateinit var btn_logout: Button
    var tokens = ""

    lateinit var full_name_header: TextView
    lateinit var nickname_header: TextView
    lateinit var domain: TextView
    lateinit var name: TextView
    lateinit var email: TextView
    lateinit var nomor_handphone: TextView
    lateinit var password: TextView

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        mView = inflater.inflate(R.layout.fragment_account, container, false)


        authPresenter.setupAuthView(this)
        sharedPreference = SharedPreference(requireActivity())

        tokens    = sharedPreference.getValueString(getString(R.string.PREF_TOKEN)).toString()
        d("gettokenid", tokens)
        authPresenter.doProfile("Bearer $tokens")

        full_name_header  = mView.findViewById(R.id.tv_full_name_header)
        nickname_header   = mView.findViewById(R.id.tv_nickname_header)
        domain          = mView.findViewById(R.id.tv_domain)
        name            = mView.findViewById(R.id.tv_nama)
        email           = mView.findViewById(R.id.tv_email)
        nomor_handphone = mView.findViewById(R.id.tv_handphone)

        btn_logout = mView.findViewById(R.id.btn_logout)
        btn_logout.setOnClickListener {
            MaterialAlertDialogBuilder(requireContext())
                .setTitle("NOTIF")
                .setMessage("Apakah anda yakin ingin keluar?")
                .setNegativeButton("Batal") { dialog, _ ->
                    dialog.dismiss()
                }
                .setPositiveButton("Keluar") { dialog, _ ->
                    sharedPreference.save(getString(R.string.PREF_SESSION), false)
                    sharedPreference.save(getString(R.string.PREF_TOKEN), "")
                    sharedPreference.save(getString(R.string.PREF_REFRESH_TOKEN), "")
                    val intent = Intent(requireContext(), StartedActivity::class.java)
                    startActivity(intent)
                }
                .show()
        }


        return mView
    }

    override fun whenSuccessLogin(success: Boolean?, data: DataResponAuth) {
        TODO("Not yet implemented")
    }

    override fun whenSuccessVerifikasi(success: Boolean?, data: DataResponAuth) {
        TODO("Not yet implemented")
    }

    override fun whenSuccessRegister(success: Boolean?, data: DataResponAuth) {
        TODO("Not yet implemented")
    }

    override fun whenSuccessForgotPassword(status: Boolean?, data: ResponOtp) {
        TODO("Not yet implemented")
    }

    override fun whenSuccessOtp(status: Boolean?, message: String?, data: DataResponOtp?) {
        TODO("Not yet implemented")
    }

    override fun whenSuccessProfile(success: Boolean?, data: DataResponProfile) {
        try {
            full_name_header.text = data.full_name
            nickname_header.text  = data.full_name
            domain.text           = "domain.my.id"
            name.text             = data.nickname
            email.text            = data.email
            nomor_handphone.text  = data.phone_number
        } catch (e: ApiException){
            Toast.makeText(requireContext(),e.toString(), Toast.LENGTH_SHORT).show()
        }
    }

    override fun whenHandleError(success: Boolean?, error: String?) {
        TODO("Not yet implemented")
    }

    override fun showProgress() {
        progressDialog.show(requireContext(),"")
    }

    override fun hideProgress() {
        progressDialog.dialog_custom.dismiss()
    }

}