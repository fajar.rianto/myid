package com.example.myid.ui.auth

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log.d
import android.widget.Toast
import com.example.myid.core.models.auth.DataResponAuth
import com.example.myid.core.models.otp.DataResponOtp
import com.example.myid.core.models.otp.ResponOtp
import com.example.myid.core.models.profile.DataResponProfile
import com.example.myid.core.presenter.auth.AuthPresenter
import com.example.myid.core.presenter.auth.AuthView
import com.example.myid.databinding.ActivityNewPasswordBinding
import com.example.myid.extentions.CustomProgressDialogFull
import com.google.android.gms.common.api.ApiException

class NewPasswordActivity : AppCompatActivity(), AuthView.View.auth {
    private lateinit var binding: ActivityNewPasswordBinding
    private var authPresenter = AuthPresenter()
    private val progressDialog = CustomProgressDialogFull()

    var get_phome  = ""
    var get_token  = ""
    var get_password = ""
    var get_confirm_password = ""
    var message_password = ""
    var is_valid  = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityNewPasswordBinding.inflate(layoutInflater)
        setContentView(binding.root)


        authPresenter.setupAuthView(this)
        get_phome = intent.getStringExtra("phone").toString()
        get_token = intent.getStringExtra("token").toString()


        binding.btnSimpan.setOnClickListener {
            get_password = binding.txNewPassword.text.toString()
            get_confirm_password = binding.txNewPassword.text.toString()
            d("testpassword", get_password)
            isValidPassword(get_password)
            if (is_valid) {
                if (get_password == get_confirm_password){
                    authPresenter.doSendNewForgotPassword(get_password, get_phome, get_token)
                }else{
                    Toast.makeText(this, "Password tidak sama", Toast.LENGTH_SHORT).show()
                }
            } else {
                Toast.makeText(this, "$message_password", Toast.LENGTH_SHORT).show()
            }
        }
    }

    fun isValidPassword(password: String) {

        val validate_uppercase = ".*[A-Z].*".toRegex()
        val validate_digit = ".*[0-9].*".toRegex()
        val validate_caracter = ".*[@#!$%^&+=].*".toRegex()

        val is_upper = password.matches(validate_uppercase)
        val is_digit = password.matches(validate_digit)
        val is_caracter = password.matches(validate_caracter)

        if (password.length < 6) {
            message_password = "Password lenght must have alleast 6 character !!"
            is_valid = false
        } else {
            if (is_upper) {
                if (is_digit) {
                    if (is_caracter) {
                        is_valid = true
                        message_password = "password secure is good"
                    } else {
                        message_password = "Password must have special characters !!"
                        is_valid = false
                    }
                } else {
                    is_valid = false
                    message_password = "Password must have atleast one digit character !!"
                }
            } else {
                is_valid = false
                message_password = "Password must have atleast one uppercase character !!"
            }
        }
    }
    override fun whenSuccessLogin(success: Boolean?, data: DataResponAuth) {
        TODO("Not yet implemented")
    }

    override fun whenSuccessVerifikasi(success: Boolean?, data: DataResponAuth) {
        TODO("Not yet implemented")
    }

    override fun whenSuccessRegister(success: Boolean?, data: DataResponAuth) {
        TODO("Not yet implemented")
    }

    override fun whenSuccessForgotPassword(status: Boolean?, data: ResponOtp) {
        TODO("Not yet implemented")
    }

    override fun whenSuccessOtp(status: Boolean?, message: String?, data: DataResponOtp?) {
        try {
            val intent = Intent(this, NewPasswordSuccessActivity::class.java)
            startActivity(intent)
        } catch (e: ApiException){
            Toast.makeText(this,e.toString(), Toast.LENGTH_SHORT).show()
        }
    }

    override fun whenSuccessProfile(success: Boolean?, data: DataResponProfile) {
        TODO("Not yet implemented")
    }

    override fun whenHandleError(success: Boolean?, error: String?) {
        TODO("Not yet implemented")
    }

    override fun showProgress() {
        progressDialog.show(this,"")
    }

    override fun hideProgress() {
        progressDialog.dialog_custom.dismiss()
    }
}