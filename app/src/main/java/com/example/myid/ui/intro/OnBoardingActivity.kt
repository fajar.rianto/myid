package com.example.myid.ui.intro

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.viewpager2.widget.ViewPager2
import androidx.viewpager2.widget.ViewPager2.OnPageChangeCallback
import com.example.mettadc.core.models.intro.OnBoardingItem
import com.example.mettadc.ui.intro.OnBoardingAdapter
import com.example.myid.R
import com.example.myid.databinding.ActivityIntroBinding
import com.example.myid.extentions.CustomProgressDialogFull
import com.example.myid.extentions.SharedPreference

class OnBoardingActivity : AppCompatActivity() {
    private lateinit var binding: ActivityIntroBinding

    private lateinit var sharedPreference: SharedPreference
    private val progressDialog = CustomProgressDialogFull()

    private var onboardingAdapter: OnBoardingAdapter? = null
    private var layoutOnboardingIndicator: LinearLayout? = null
    var is_onboarding = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityIntroBinding.inflate(layoutInflater)
        setContentView(binding.root)

        sharedPreference = SharedPreference(this)
        is_onboarding    = sharedPreference.getValueBoolien(getString(R.string.PREF_ONBOARDING), false)
        if (is_onboarding){
            startActivity(Intent(applicationContext, StartedActivity::class.java))
            finish()
        }

        layoutOnboardingIndicator = binding.layoutOnboardingIndicators

        setOnboardingItem()
        val onboardingViewPager = findViewById<ViewPager2>(R.id.screen_viewpager)
        onboardingViewPager.adapter = onboardingAdapter
        setOnboadingIndicator()
        setCurrentOnboardingIndicators(0)
        onboardingViewPager.registerOnPageChangeCallback(object : OnPageChangeCallback() {
            override fun onPageSelected(position: Int) {
                super.onPageSelected(position)
                setCurrentOnboardingIndicators(position)
            }
        })


        binding.buttonOnBoardingAction.setOnClickListener {
            if (onboardingViewPager.currentItem + 1 < onboardingAdapter!!.itemCount) {
                onboardingViewPager.currentItem = onboardingViewPager.currentItem + 1
            } else {
                sharedPreference.save(getString(R.string.PREF_ONBOARDING), true)
                startActivity(Intent(applicationContext, StartedActivity::class.java))
                finish()
            }
        }
    }

    private fun setOnboadingIndicator() {
        val indicators = arrayOfNulls<ImageView>(onboardingAdapter!!.itemCount)
        val layoutParams = LinearLayout.LayoutParams(
            ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT
        )
        layoutParams.setMargins(8, 0, 8, 0)
        for (i in indicators.indices) {
            indicators[i] = ImageView(applicationContext)
            indicators[i]!!.setImageDrawable(
                ContextCompat.getDrawable(
                    applicationContext, R.drawable.shape
                )
            )
            indicators[i]!!.layoutParams = layoutParams
            layoutOnboardingIndicator!!.addView(indicators[i])
        }
    }

    @SuppressLint("SetTextI18n")
    private fun setCurrentOnboardingIndicators(index: Int) {
        val childCount = layoutOnboardingIndicator!!.childCount
        for (i in 0 until childCount) {
            val imageView = layoutOnboardingIndicator!!.getChildAt(i) as ImageView
            if (i == index) {
                imageView.setImageDrawable(
                    ContextCompat.getDrawable(
                        applicationContext,
                        R.drawable.shape
                    )
                )
            } else {
                imageView.setImageDrawable(
                    ContextCompat.getDrawable(
                        applicationContext,
                        R.drawable.shape
                    )
                )
            }
        }
        if (index == onboardingAdapter!!.itemCount - 1) {
            binding.buttonOnBoardingAction!!.text = "Get Started"
        } else {
            binding.buttonOnBoardingAction!!.text = "Next"
        }
    }

    private fun setOnboardingItem() {
        val onBoardingItems: MutableList<OnBoardingItem> = ArrayList()
        val itemboard1 = OnBoardingItem()
        itemboard1.title = "All About You"
        itemboard1.description = "Memudahkan Berkenalanmu"
        itemboard1.image = R.drawable.splash


        val itemboard2 = OnBoardingItem()
        itemboard2.title = "Email"
        itemboard2.description = "dengan my.id di belakangnya"
        itemboard2.image = R.drawable.dynamic

        val itemboard3 = OnBoardingItem()
        itemboard3.title = "Biolink"
        itemboard3.description = "Mudahnya memiliki biolink kamu"
        itemboard3.image = R.drawable.biolink

        val itemboard4 = OnBoardingItem()
        itemboard4.title = "Resume"
        itemboard4.description = "Sungguh luar biasa"
        itemboard4.image = R.drawable.gradient

        onBoardingItems.add(itemboard1)
        onBoardingItems.add(itemboard2)
        onBoardingItems.add(itemboard3)
        onBoardingItems.add(itemboard4)
        onboardingAdapter = OnBoardingAdapter(onBoardingItems)
    }
}