package com.example.myid.extentions


interface BaseView {
    interface progressView{
        fun showProgress()
        fun hideProgress()
    }
}