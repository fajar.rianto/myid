package com.example.myid.extentions

import android.annotation.SuppressLint
import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.os.Build
import android.view.WindowInsets
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.constraintlayout.widget.ConstraintLayout
import com.example.myid.R


class CustomProgressDialog {
    lateinit var dialog_custom: CustomDialog

    fun show(context: Context): Dialog {
        return show(context, null)
    }

    @SuppressLint("InflateParams")
    fun show(context: Context, title: CharSequence?): Dialog {
        val inflater = (context as Activity).layoutInflater
        val view = inflater.inflate(R.layout.progress_dialog_view, null)
        val title_componen = view.findViewById<TextView>(R.id.cp_title)
        if (title != null) {
            title_componen.text = title
        }

        title_componen.setTextColor(Color.WHITE)

        dialog_custom = CustomDialog(context)
        dialog_custom.setContentView(view)
        dialog_custom.show()


        return dialog_custom
    }

    class CustomDialog(context: Context) : Dialog(context, R.style.CustomDialogTheme) {
        init {
            when {
                Build.VERSION.SDK_INT >= Build.VERSION_CODES.R -> {
                    window?.decorView?.rootWindowInsets?.getInsetsIgnoringVisibility(WindowInsets.Type.systemBars())?.bottom
                }
            }
        }
    }
}